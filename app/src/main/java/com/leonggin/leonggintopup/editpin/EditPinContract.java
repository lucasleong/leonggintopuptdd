package com.leonggin.leonggintopup.editpin;

public interface EditPinContract {
    interface View {

        void savePins(String telekomPin, String bmobilePin);

        void setPresenter(EditPinContract.Presenter editPinPresenter);

        void showDoTopup();
    }

    interface Presenter {

        void savePins(String telekomPin, String bmobilePin);

        void onPinsSaved();
    }
}
