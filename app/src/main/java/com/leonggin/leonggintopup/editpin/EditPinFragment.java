package com.leonggin.leonggintopup.editpin;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.leonggin.leonggintopup.R;

import static android.content.Context.MODE_PRIVATE;

public class EditPinFragment extends Fragment implements EditPinContract.View {
    private EditPinContract.Presenter presenter;

    private SharedPreferences sharedPreferences;

    private EditText telekomPin;
    private EditText bmobilePin;

    public static EditPinFragment newInstance() {
        return new EditPinFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_editpin, container, false);

        telekomPin = root.findViewById(R.id.telekom_pin);
        bmobilePin = root.findViewById(R.id.bmobile_pin);

        // Let fragment receive appbar touch events
        setHasOptionsMenu(true);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        sharedPreferences = getActivity().getSharedPreferences("PINS", MODE_PRIVATE);

        telekomPin.setText(sharedPreferences.getString(getString(R.string.telekom), ""));
        bmobilePin.setText(sharedPreferences.getString(getString(R.string.bmobile), ""));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_editpin, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.save) {
            presenter.savePins(
                    telekomPin.getText().toString(),
                    bmobilePin.getText().toString());
        }

        return super.onOptionsItemSelected(item);
    }

    public void setPresenter(EditPinContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showDoTopup() {
        getActivity().finish();
    }

    @Override
    public void savePins(String telekomPin, String bmobilePin) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(getString(R.string.telekom), telekomPin);
        editor.putString(getString(R.string.bmobile), bmobilePin);
        editor.apply();
        presenter.onPinsSaved();
    }
}
