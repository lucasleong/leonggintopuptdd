package com.leonggin.leonggintopup.editpin;

public class EditPinPresenter implements EditPinContract.Presenter {

    private EditPinContract.View view;

    public EditPinPresenter(EditPinContract.View view) {
        this.view = view;
        view.setPresenter(this);
    }

    @Override
    public void savePins(String telekomPin, String bmobilePin) {
        view.savePins(telekomPin, bmobilePin);
    }

    @Override
    public void onPinsSaved() {
        view.showDoTopup();
    }
}
