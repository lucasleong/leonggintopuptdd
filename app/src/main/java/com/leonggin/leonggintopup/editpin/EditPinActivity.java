package com.leonggin.leonggintopup.editpin;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.leonggin.leonggintopup.R;

public class EditPinActivity extends AppCompatActivity {

    private EditPinPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editpin);

        // Custom toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.edit_pin);

        // Create fragment
        EditPinFragment fragment =
                (EditPinFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (fragment == null) {
            fragment = EditPinFragment.newInstance();
            initFragment(fragment);
        }

        // Create presenter
        presenter = new EditPinPresenter(fragment);
    }

    private void initFragment(EditPinFragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment, fragment);
        transaction.commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
