package com.leonggin.leonggintopup.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Topup {

    private long id;
    private String number;
    private String amount;
    private String time;
    private boolean completed;

    public Topup(String number, String amount) {
        this.number = number;
        this.amount = amount;
        time = getTodaysDateAsString();
        completed = false;
    }

    public Topup(long id, String number, String amount, String time, int completed) {
        this.id = id;
        this.number = number;
        this.amount = amount;
        this.time = time;
        this.completed = convertCompletedFromIntToBoolean(completed);
    }

    private boolean convertCompletedFromIntToBoolean(int completed) {
        return completed == 1;
    }

    private String getTodaysDateAsString() {
        return new SimpleDateFormat("dd/MM/yyyy", Locale.CHINESE).format(new Date());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public boolean getCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Topup) {
            Topup topupDetails = (Topup) o;
            if (number.equals(topupDetails.number) && amount.equals(topupDetails.amount)) {
                return true;
            }
        }

        return false;
    }
}
