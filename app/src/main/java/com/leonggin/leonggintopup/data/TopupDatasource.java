package com.leonggin.leonggintopup.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

public class TopupDatasource {

    private static final String SQL_CREATE_NUMBER_TABLE =
            "CREATE TABLE " +
                    NumberEntry.TABLE_NAME + " (" +
                    NumberEntry._ID + " INTEGER PRIMARY KEY," +
                    NumberEntry.COLUMN_NAME_NUMBER + " TEXT NOT NULL UNIQUE, CHECK(length(" + NumberEntry.COLUMN_NAME_NUMBER + ") = 7))";

    private static final String SQL_DELETE_NUMBER_TABLE =
            "DROP TABLE IF EXISTS " + NumberEntry.TABLE_NAME;

    private static final String SQL_CREATE_TOPUP_TABLE =
            "CREATE TABLE " +
                    TopupEntry.TABLE_NAME + " (" +
                    TopupEntry._ID + " INTEGER PRIMARY KEY," +
                    TopupEntry.COLUMN_NAME_NUMBER_ID + " INTEGER NOT NULL," +
                    TopupEntry.COLUMN_NAME_AMOUNT + " TEXT NOT NULL," +
                    TopupEntry.COLUMN_NAME_TIME + " TEXT NOT NULL," +
                    TopupEntry.COLUMN_NAME_COMPLETED + " INTEGER NOT NULL," +
                    "CONSTRAINT tbl_topup_number_id_fk_tbl_number_id " +
                    "FOREIGN KEY (" + TopupEntry.COLUMN_NAME_NUMBER_ID + ") " +
                    "REFERENCES " + NumberEntry.TABLE_NAME + "(" + NumberEntry._ID + "))";

    private static final String SQL_DELETE_TOPUP_TABLE =
            "DROP TABLE IF EXISTS " + TopupEntry.TABLE_NAME;

    private static final String SQL_GET_ALL_TOPUPS =
            "SELECT * FROM " + NumberEntry.TABLE_NAME + " num JOIN " + TopupEntry.TABLE_NAME + " tu " +
                    "ON num." + NumberEntry._ID + " = tu." + TopupEntry.COLUMN_NAME_NUMBER_ID + " ORDER BY num." + NumberEntry._ID + " DESC LIMIT 200";

    private static final String SQL_SEARCH_TOPUPS =
            "SELECT * FROM " + NumberEntry.TABLE_NAME + " num JOIN " + TopupEntry.TABLE_NAME + " tu " +
                    "ON num." + NumberEntry._ID + " = tu." + TopupEntry.COLUMN_NAME_NUMBER_ID +
                    " WHERE num." + NumberEntry.COLUMN_NAME_NUMBER + " LIKE ?";

    private static final String SQL_DELETE_TOPUP =
            "DELETE FROM " + TopupEntry.TABLE_NAME +
                    " WHERE " + TopupEntry._ID + " = ?";

    private TopupDatasource() {
    }

    public static class NumberEntry implements BaseColumns {
        public static final String TABLE_NAME = "number";
        public static final String COLUMN_NAME_NUMBER = "number";
    }

    public static class TopupEntry implements BaseColumns {
        public static final String TABLE_NAME = "topup";
        public static final String COLUMN_NAME_NUMBER_ID = "number_id";
        public static final String COLUMN_NAME_AMOUNT = "amount";
        public static final String COLUMN_NAME_TIME = "time";
        public static final String COLUMN_NAME_COMPLETED = "completed";
    }

    public static class TopupDbHelper extends SQLiteOpenHelper {

        public static final int DATABASE_VERSION = 1;
        public static final String DATABASE_NAME = "TopupDatasource.db";

        private static TopupDbHelper instance;

        private TopupDbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        public static synchronized TopupDbHelper getInstance(Context context) {
            if (instance == null) {
                instance = new TopupDbHelper(context.getApplicationContext());
            }

            return instance;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_NUMBER_TABLE);
            db.execSQL(SQL_CREATE_TOPUP_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(SQL_DELETE_NUMBER_TABLE);
            db.execSQL(SQL_DELETE_TOPUP_TABLE);
            onCreate(db);
        }

        @Override
        public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onUpgrade(db, oldVersion, newVersion);
        }

        // TODO NEED TO FIX THIS UP
        public void insertTopup(Topup topup) {
            SQLiteDatabase db = getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(NumberEntry.COLUMN_NAME_NUMBER, topup.getNumber());

            long numberId = db.insert(NumberEntry.TABLE_NAME, null, values);

            if (ifTopupNumberIsAlreadyInDatabase(numberId)) {
                String[] projection = {NumberEntry._ID};
                String selection = NumberEntry.COLUMN_NAME_NUMBER + " = ?";
                String[] selectionArgs = {String.valueOf(topup.getNumber())};

                Cursor cursor = db.query(NumberEntry.TABLE_NAME, projection, selection,
                        selectionArgs, null, null, null);

                while (cursor.moveToNext()) {
                    numberId = cursor.getLong(cursor.getColumnIndexOrThrow(NumberEntry._ID));
                }
                cursor.close();
            }

            values.clear();
            values.put(TopupEntry.COLUMN_NAME_NUMBER_ID, numberId);
            values.put(TopupEntry.COLUMN_NAME_AMOUNT, topup.getAmount());
            values.put(TopupEntry.COLUMN_NAME_COMPLETED, topup.getCompleted());
            values.put(TopupEntry.COLUMN_NAME_TIME, topup.getTime());

            topup.setId(db.insert(TopupEntry.TABLE_NAME, null, values));
        }

        private boolean ifTopupNumberIsAlreadyInDatabase(long numberId) {
            return numberId == -1;
        }

        public List<Topup> getAllTopups() {
            SQLiteDatabase db = getWritableDatabase();

            List<Topup> topupList = new ArrayList<>();

            Cursor cursor = db.rawQuery(SQL_GET_ALL_TOPUPS, new String[]{});
            loadTopupsIntoList(topupList, cursor);

            return topupList;
        }

        public List<Topup> searchNumber(String numToSearch) {
            SQLiteDatabase db = getWritableDatabase();

            List<Topup> topupList = new ArrayList<>();

            Cursor cursor = db.rawQuery(SQL_SEARCH_TOPUPS, new String[]{numToSearch + "%"});
            loadTopupsIntoList(topupList, cursor);

            return topupList;
        }

        public void deleteTopup(long topupId) {
            SQLiteDatabase db = getWritableDatabase();

            db.delete(TopupEntry.TABLE_NAME, TopupEntry._ID + " = " + topupId, null);
        }

        private void loadTopupsIntoList(List<Topup> topupList, Cursor cursor) {
            while (cursor.moveToNext()) {
                long id = cursor.getLong(cursor.getColumnIndexOrThrow(TopupEntry._ID));
                String number = cursor.getString(cursor.getColumnIndexOrThrow(NumberEntry.COLUMN_NAME_NUMBER));
                String amount = cursor.getString(cursor.getColumnIndexOrThrow(TopupEntry.COLUMN_NAME_AMOUNT));
                String time = cursor.getString(cursor.getColumnIndexOrThrow(TopupEntry.COLUMN_NAME_TIME));
                int completed = cursor.getInt(cursor.getColumnIndexOrThrow(TopupEntry.COLUMN_NAME_COMPLETED));

                Topup topup = new Topup(id, number, amount, time, completed);
                topupList.add(topup);
            }
            cursor.close();
        }
    }
}
