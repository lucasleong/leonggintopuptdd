package com.leonggin.leonggintopup.data;

import android.util.Log;

import java.util.List;

public class TopupRepository {
    private List<Topup> topupList;
    private Topup cachedTopup;
    private TopupDatasource.TopupDbHelper dbHelper;

    private static TopupRepository instance;

    public static TopupRepository getInstance(TopupDatasource.TopupDbHelper dbHelper) {
        if (instance == null) {
            instance = new TopupRepository(dbHelper);
        }

        return instance;
    }

    private TopupRepository(TopupDatasource.TopupDbHelper dbHelper) {
        this.dbHelper = dbHelper;
        topupList = dbHelper.getAllTopups();
    }

    public List<Topup> getAllTopups() {
        while(topupList.size() > 200) {
            topupList.remove(0);
        }

        return topupList;
    }

    public Topup getCachedTopup() {
        return cachedTopup;
    }

    public void cacheUncompletedTopup(Topup topupList) {
        cachedTopup = topupList;
    }

    public void completeCachedTopup() {
        cachedTopup.setCompleted(true);
        dbHelper.insertTopup(cachedTopup);
        if (topupList != null) {
            topupList.add(cachedTopup);
        }
    }

    public void closeDb() {
        dbHelper.close();
    }

    public List<Topup> searchTopup(String search) {
        return dbHelper.searchNumber(search);
    }

    public void deleteTopup(long topupId) {
        dbHelper.deleteTopup(topupId);
        topupList = dbHelper.getAllTopups();
    }
}
