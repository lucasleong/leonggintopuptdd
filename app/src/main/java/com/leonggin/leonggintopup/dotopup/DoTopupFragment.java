package com.leonggin.leonggintopup.dotopup;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.telecom.PhoneAccountHandle;
import android.telecom.TelecomManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.leonggin.leonggintopup.R;
import com.leonggin.leonggintopup.data.Topup;
import com.leonggin.leonggintopup.editpin.EditPinActivity;

import java.util.List;

import static android.content.Context.MODE_PRIVATE;

public class DoTopupFragment extends Fragment implements DoTopupContract.View {
    private final int REQUEST_PERMISSION_PHONE_STATE = 500;

    private DoTopupContract.Presenter presenter;

    private SharedPreferences sharedPreferences;

    private EditText number;
    private EditText amount;

    public static DoTopupFragment newInstance() {
        return new DoTopupFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_dotopup, container, false);

        number = root.findViewById(R.id.number);
        amount = root.findViewById(R.id.amount);

        // Let fragment receive appbar touch events
        setHasOptionsMenu(true);

        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        sharedPreferences = getActivity().getSharedPreferences("PINS", MODE_PRIVATE);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_dotopup, menu);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.topup:
                // Hide keyboard
                InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);

                onTopupBtnClicked();
                break;
            case R.id.editpin:
                presenter.onEditPinBtnClicked();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onTopupBtnClicked() {
        String telekomPin = sharedPreferences.getString(getString(R.string.telekom), "");
        String bmobilePin = sharedPreferences.getString(getString(R.string.bmobile), "");
        if (telekomPin.equals("") || bmobilePin.equals("")) {
            Snackbar.make(getView(), getString(R.string.pins_must_be_set_error), Snackbar.LENGTH_LONG).show();
        } else {
            presenter.onTopupBtnClicked(
                    number.getText().toString(),
                    amount.getText().toString(),
                    sharedPreferences.getString(getString(R.string.telekom), ""),
                    sharedPreferences.getString(getString(R.string.bmobile), ""));
        }
    }

    @Override
    public void doTopup(Topup topup, Uri topupUri) {
//        String topupString = "tel:*247*0*" + topup.getNumber() + "*" + topup.getAmount() + "*6382" + Uri.encode("#");
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(topupUri);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            TelecomManager telecomManager = (TelecomManager) getContext().getSystemService(Context.TELECOM_SERVICE);
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.READ_PHONE_STATE}, REQUEST_PERMISSION_PHONE_STATE);
                return;
            }

            intent.putExtra("com.android.phone.force.slot", true);
            List<PhoneAccountHandle> phoneAccountHandleList = telecomManager.getCallCapablePhoneAccounts();
            if (topup.getNumber().startsWith("7")) {
                intent.putExtra("com.android.phone.extra.slot", 0);
                if (phoneAccountHandleList != null && phoneAccountHandleList.size() > 0)
                    intent.putExtra("android.telecom.extra.PHONE_ACCOUNT_HANDLE", phoneAccountHandleList.get(0));
            } else if (topup.getNumber().startsWith("8")) {
                intent.putExtra("com.android.phone.extra.slot", 1);
                if (phoneAccountHandleList != null && phoneAccountHandleList.size() > 1)
                    intent.putExtra("android.telecom.extra.PHONE_ACCOUNT_HANDLE", phoneAccountHandleList.get(1));
            }
        }

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CALL_PHONE}, 22);
        } else {
            startActivityForResult(intent, 22);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_PERMISSION_PHONE_STATE:
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onTopupBtnClicked();
                }
        }
    }

    @Override
    public void setPresenter(DoTopupContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showTopupHistory() {
        getActivity().finish();
    }

    @Override
    public void numberMustBe7CharactersLongError() {
        number.setError(getString(R.string.number_must_be_7_characters));
    }

    @Override
    public void numberMustStartWithA7OrAn8() {
        number.setError(getString(R.string.number_must_start_with_a_7_or_an_8));
    }

    @Override
    public void showEditPin() {
        Intent intent = new Intent(getActivity(), EditPinActivity.class);
        startActivity(intent);
    }

    @Override
    public void amountMustBe5DollarsOrMoreError() {
        amount.setError(getString(R.string.amount_must_be_5_dollars_or_more));
    }

    @Override
    public void amountMustBe1000DollarsOrLessError() {
        amount.setError(getString(R.string.amount_must_be_1000_dollars_or_less));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        presenter.onTopupCompleted();
    }
}
