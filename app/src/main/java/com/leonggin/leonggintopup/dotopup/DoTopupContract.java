package com.leonggin.leonggintopup.dotopup;

import android.net.Uri;

import com.leonggin.leonggintopup.data.Topup;

public interface DoTopupContract {
    interface View {
        void doTopup(Topup topup, Uri topupUri);

        void setPresenter(Presenter presenter);

        void showTopupHistory();

        void numberMustBe7CharactersLongError();

        void amountMustBe5DollarsOrMoreError();

        void amountMustBe1000DollarsOrLessError();

        void numberMustStartWithA7OrAn8();

        void showEditPin();
    }

    interface Presenter {
        void onTopupCompleted();

        void onTopupBtnClicked(String number, String amount, String telekomPin, String bmobilePin);

        void onDestroy();

        void onEditPinBtnClicked();
    }
}
