package com.leonggin.leonggintopup.dotopup;

import android.net.Uri;

import com.leonggin.leonggintopup.data.Topup;
import com.leonggin.leonggintopup.data.TopupRepository;

public class DoTopupPresenter implements DoTopupContract.Presenter {

    private DoTopupContract.View view;
    private TopupRepository repository;

    public DoTopupPresenter(DoTopupContract.View view, TopupRepository repository) {
        this.view = view;
        view.setPresenter(this);
        this.repository = repository;
    }

    @Override
    public void onTopupCompleted() {
        repository.completeCachedTopup();
        view.showTopupHistory();
    }

    @Override
    public void onTopupBtnClicked(String number, String amount, String telekomPin, String bmobilePin) {
        if (isValidInput(number, amount)) {
            Topup topup = new Topup(number, amount);
            repository.cacheUncompletedTopup(topup);
            view.doTopup(topup, getTopupUri(topup, telekomPin, bmobilePin));
        }
    }

    @Override
    public void onDestroy() {
        repository.closeDb();
    }

    @Override
    public void onEditPinBtnClicked() {
        view.showEditPin();
    }

    private boolean isValidInput(String number, String amount) {
        boolean isValid = true;
        if (number.equals("") || number.length() < 7 || number.length() > 7) {
            isValid = false;
            view.numberMustBe7CharactersLongError();
        } else if (!number.startsWith("7") && !number.startsWith("8")) {
            isValid = false;
            view.numberMustStartWithA7OrAn8();
        }

        if (amount.equals("") || Integer.parseInt(amount) < 5) {
            isValid = false;
            view.amountMustBe5DollarsOrMoreError();
        } else if (Integer.parseInt(amount) > 1000) {
            isValid = false;
            view.amountMustBe1000DollarsOrLessError();
        }

        return isValid;
    }

    private Uri getTopupUri(Topup topup, String telekomPin, String bmobilePin) {
        StringBuilder topupUri = new StringBuilder();
        topupUri.append("tel:*");
        if (topup.getNumber().charAt(0) == '7') {
            topupUri.append("247");
        } else if (topup.getNumber().charAt(0) == '8') {
            topupUri.append("123");
        }
        topupUri.append("*0*");
        topupUri.append(topup.getNumber());
        topupUri.append("*");
        topupUri.append(topup.getAmount());
        topupUri.append("*");
        if (topup.getNumber().charAt(0) == '7') {
            topupUri.append(telekomPin);
        } else if (topup.getNumber().charAt(0) == '8') {
            topupUri.append(bmobilePin);
        }
        topupUri.append(Uri.encode("#"));
        return Uri.parse(topupUri.toString());
    }
}
