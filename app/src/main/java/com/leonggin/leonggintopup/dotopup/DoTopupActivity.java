package com.leonggin.leonggintopup.dotopup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.leonggin.leonggintopup.R;
import com.leonggin.leonggintopup.data.TopupDatasource;
import com.leonggin.leonggintopup.data.TopupRepository;

public class DoTopupActivity extends AppCompatActivity {

    private DoTopupContract.Presenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dotopup);

        // Custom toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.do_topup);

//        overridePendingTransition(R.anim.translate_left_in, R.anim.translate_left_out);

        // Create fragment
        DoTopupFragment fragment =
                (DoTopupFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (fragment == null) {
            fragment = DoTopupFragment.newInstance();
            initFragment(fragment);
        }

        // Load data source
        TopupDatasource.TopupDbHelper dbHelper = TopupDatasource.TopupDbHelper.getInstance(getApplicationContext());

        // Create presenter
        presenter = new DoTopupPresenter(fragment, TopupRepository.getInstance(dbHelper));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    private void initFragment(DoTopupFragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment, fragment);
        transaction.commit();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
