package com.leonggin.leonggintopup.topup;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.leonggin.leonggintopup.R;
import com.leonggin.leonggintopup.data.Topup;
import com.leonggin.leonggintopup.data.TopupDatasource;
import com.leonggin.leonggintopup.data.TopupRepository;
import com.leonggin.leonggintopup.dotopup.DoTopupActivity;

import java.util.List;

public class TopupActivity extends AppCompatActivity implements TopupContract.View, TopupHistoryItemTouchHelper.TopupHistoryItemTouchHelperListener {
    private static final String TAG = "TopupActivity";

    private TopupContract.Presenter presenter;
    private TopupHistoryFragment fragment;

    private FloatingActionButton doTopupFab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topup);

        // Custom toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.topup_history);

        // Load data source
//        getApplicationContext().deleteDatabase(TopupDatasource.TopupDbHelper.DATABASE_NAME); // TODO REMOVE
        TopupDatasource.TopupDbHelper dbHelper = TopupDatasource.TopupDbHelper.getInstance(getApplicationContext());

        // Create presenter
        presenter = new TopupPresenter(this, TopupRepository.getInstance(dbHelper));

        // Create fragment
        fragment = (TopupHistoryFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        if (fragment == null) {
            fragment = TopupHistoryFragment.newInstance();
            initFragment(fragment);
        }

        // Do topup floating action button
        doTopupFab = findViewById(R.id.dotop);
        doTopupFab.setOnClickListener(view -> {
            presenter.onDoTopupBtnClicked();
            doTopupFab.setEnabled(false);
        });
    }

    private void initFragment(TopupHistoryFragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.fragment, fragment);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
        doTopupFab.setEnabled(true);
//        overridePendingTransition(R.anim.translate_right_in, R.anim.translate_right_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_topup, menu);

        MenuItem searchBtn = menu.findItem(R.id.menu_topup);
        searchBtn.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                doTopupFab.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                doTopupFab.setVisibility(View.VISIBLE);
                return true;
            }
        });

        SearchView searchView = (SearchView) menu.findItem(R.id.menu_topup).getActionView();
        searchView.setInputType(InputType.TYPE_CLASS_NUMBER);
        searchView.setQueryHint(getString(R.string.search_number));

        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            public boolean onQueryTextChange(String number) {
                presenter.onSearchNumber(number);
                return true;
            }

            public boolean onQueryTextSubmit(String number) {
                presenter.onSearchNumber(number);
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void refreshHistory(List<Topup> topupHistory) {
        fragment.update(topupHistory);
    }

    @Override
    public void showDoTopupView() {
        Intent intent = new Intent(this, DoTopupActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, long topupId, int position) {
        presenter.onTopupItemSwiped(topupId, position);
    }

    @Override
    public void onItemRemoved(int position) {
        fragment.removeItem(position);
    }
}
