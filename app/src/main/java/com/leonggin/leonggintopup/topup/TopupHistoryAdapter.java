package com.leonggin.leonggintopup.topup;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.leonggin.leonggintopup.R;
import com.leonggin.leonggintopup.data.Topup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TopupHistoryAdapter extends RecyclerView.Adapter<TopupHistoryAdapter.ViewHolder> {

    private List<Topup> topupHistory;

    public TopupHistoryAdapter(ArrayList<Topup> topupHistory) {
        this.topupHistory = topupHistory;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_topup, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public int getItemCount() {
        return topupHistory.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Topup topup = topupHistory.get(position);
        holder.getNumber().setText(String.valueOf(topup.getNumber()));
        holder.getAmount().setText("$" + String.valueOf(topup.getAmount()));
        holder.getDate().setText(String.valueOf(topup.getTime()));
        holder.setTopupId(topup.getId());
    }

    public void removeItem(int position) {
        topupHistory.remove(position);
        notifyItemRemoved(position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView number;
        private TextView amount;
        private TextView date;
        private ConstraintLayout foreground;
        private ConstraintLayout background;
        private long topupId;

        public ViewHolder(View itemView) {
            super(itemView);

            number = itemView.findViewById(R.id.number);
            amount = itemView.findViewById(R.id.amount);
            date = itemView.findViewById(R.id.date);
            foreground = itemView.findViewById(R.id.foreground);
            background = itemView.findViewById(R.id.background);
        }

        public TextView getNumber() {
            return number;
        }

        public TextView getAmount() {
            return amount;
        }

        public TextView getDate() {
            return date;
        }

        public ConstraintLayout getForeground() {
            return foreground;
        }

        public ConstraintLayout getBackground() {
            return background;
        }

        public long getTopupId() {
            return topupId;
        }

        public void setTopupId(long topupId) {
            this.topupId = topupId;
        }
    }

    public void updateItems(List<Topup> topupHistory) {
        this.topupHistory.clear();
        this.topupHistory.addAll(topupHistory);
        Collections.reverse(this.topupHistory);
    }
}
