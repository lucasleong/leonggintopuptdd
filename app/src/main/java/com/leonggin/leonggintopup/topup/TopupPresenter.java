package com.leonggin.leonggintopup.topup;

import com.leonggin.leonggintopup.data.TopupRepository;

public class TopupPresenter implements TopupContract.Presenter {
    private TopupContract.View view;
    private TopupRepository repository;

    public TopupPresenter(TopupContract.View view, TopupRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void onDoTopupBtnClicked() {
        view.showDoTopupView();
    }

    @Override
    public void onResume() {
        view.refreshHistory(repository.getAllTopups());
    }

    @Override
    public void onDestroy() {
        repository.closeDb();
    }

    @Override
    public void onSearchNumber(String number) {
        if (number != null || !number.equals("")) {
            view.refreshHistory(repository.searchTopup(number));
        }
    }

    @Override
    public void onTopupItemSwiped(long topupId, int position) {
        repository.deleteTopup(topupId);
        view.onItemRemoved(position);
    }
}
