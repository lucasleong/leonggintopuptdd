package com.leonggin.leonggintopup.topup;

import com.leonggin.leonggintopup.data.Topup;

import java.util.List;

public interface TopupContract {

    interface View {
        void refreshHistory(List<Topup> topupHistory);

        void showDoTopupView();

        void onItemRemoved(int position);
    }

    interface Presenter {
        void onDoTopupBtnClicked();

        void onDestroy();

        void onResume();

        void onSearchNumber(String number);

        void onTopupItemSwiped(long topupId, int position);
    }
}
