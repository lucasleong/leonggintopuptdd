package com.leonggin.leonggintopup.topup;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;

public class TopupHistoryItemTouchHelper extends ItemTouchHelper.SimpleCallback {

    private TopupHistoryItemTouchHelperListener listener;

    public TopupHistoryItemTouchHelper(int dragDirs, int swipeDirs, TopupHistoryItemTouchHelperListener listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        listener.onSwiped(viewHolder, direction,
                ((TopupHistoryAdapter.ViewHolder) viewHolder).getTopupId(), viewHolder.getAdapterPosition());
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            getDefaultUIUtil().onSelected(((TopupHistoryAdapter.ViewHolder) viewHolder).getForeground());
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder,
                                float dX, float dY, int actionState, boolean isCurrentlyActive) {
        getDefaultUIUtil().onDrawOver(c, recyclerView,
                ((TopupHistoryAdapter.ViewHolder) viewHolder).getForeground(),
                dX, dY, actionState, isCurrentlyActive);
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView,
                            RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
        getDefaultUIUtil().onDraw(c, recyclerView, ((TopupHistoryAdapter.ViewHolder) viewHolder).getForeground(),
                dX, dY, actionState, isCurrentlyActive);
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        getDefaultUIUtil().clearView(((TopupHistoryAdapter.ViewHolder) viewHolder).getForeground());
    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    public interface TopupHistoryItemTouchHelperListener {
        void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, long topupId, int position);
    }
}
