package com.leonggin.leonggintopup.topup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.leonggin.leonggintopup.R;
import com.leonggin.leonggintopup.data.Topup;

import java.util.ArrayList;
import java.util.List;

public class TopupHistoryFragment extends Fragment {

    private TopupHistoryAdapter adapter;

    public static TopupHistoryFragment newInstance() {
        return new TopupHistoryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.rv_history, container, false);

        // Init recycler view
        RecyclerView recyclerView = view.findViewById(R.id.history);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.VERTICAL));

        // Init adapter
        adapter = new TopupHistoryAdapter(new ArrayList<>());
        recyclerView.setAdapter(adapter);

        // Init swipe to delete
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new TopupHistoryItemTouchHelper(0,
                ItemTouchHelper.LEFT, (TopupHistoryItemTouchHelper.TopupHistoryItemTouchHelperListener) getActivity()));
        itemTouchHelper.attachToRecyclerView(recyclerView);

        return view;
    }

    public void update(List<Topup> topupHistory) {
        adapter.updateItems(topupHistory);
        adapter.notifyDataSetChanged();
    }

    public void removeItem(int position) {
        adapter.removeItem(position);
//        adapter.notifyItemRemoved(position);
    }
}
