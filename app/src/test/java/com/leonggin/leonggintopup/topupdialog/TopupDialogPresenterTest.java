package com.leonggin.leonggintopup.topupdialog;

import com.leonggin.leonggintopup.data.Topup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TopupDialogPresenterTest {

    @Mock
    private TopupDialogContract.View view;

    private TopupDialogContract.Presenter presenter;

    @Before
    public void setUp() {
        presenter = new TopupDialogPresenter(view);
    }

    @Test
    public void onTopupBtnClicked_EmptyNumberString_ShowMissingNumberErrorMsg() {
        presenter.onTopupBtnClicked("", "6");

        verify(view).showNumberErrorMsg(TopupDialogError.EMPTY_NUMBER);
    }

    @Test
    public void onTopupBtnClicked_NumberLengthLessThan7_ShowNumberLengthErrorMsg() {
        presenter.onTopupBtnClicked("7777", "123");

        verify(view).showNumberErrorMsg(TopupDialogError.NUMBER_LENGTH_LESS_THAN_7_CHARACTERS);
    }

    @Test
    public void onTopupBtnClicked_NumberLengthMoreThan7_ShowNumberLengthErrorMsg() {
        presenter.onTopupBtnClicked("77777777", "222");

        verify(view).showNumberErrorMsg(TopupDialogError.NUMBER_LENGTH_MORE_THAN_7_CHARACTERS);
    }

    @Test
    public void onTopupBtnClicked_NumberNotStartWith7_ShowInvalidNumberErrorMsg() {
        presenter.onTopupBtnClicked("8888888", "124");

        verify(view).showNumberErrorMsg(TopupDialogError.NUMBER_MUST_START_WITH_7);
    }

    @Test
    public void onTopupBtnClicked_EmptyNumber_OnlyShowMissingNumberErrorMsg() {
        presenter.onTopupBtnClicked("", "7");

        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_LENGTH_LESS_THAN_7_CHARACTERS);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_LENGTH_MORE_THAN_7_CHARACTERS);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_MUST_START_WITH_7);
    }

    @Test
    public void onTopupBtnClicked_NumberLengthLessThan7_OnlyShowNumberLengthErrorMsg() {
        presenter.onTopupBtnClicked("78936", "7");

        verify(view, never()).showNumberErrorMsg(TopupDialogError.EMPTY_NUMBER);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_LENGTH_MORE_THAN_7_CHARACTERS);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_MUST_START_WITH_7);
    }

    @Test
    public void onTopupBtnClicked_NumberLengthMoreThan7_OnlyShowNumberLengthErrorMsg() {
        presenter.onTopupBtnClicked("7888888889", "9");

        verify(view, never()).showNumberErrorMsg(TopupDialogError.EMPTY_NUMBER);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_LENGTH_LESS_THAN_7_CHARACTERS);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_MUST_START_WITH_7);
    }

    @Test
    public void onTopupBtnClicked_NumberNotStartWith7_OnlyShowInvalidNumberErrorMsg() {
        presenter.onTopupBtnClicked("8888888", "124");

        verify(view, never()).showNumberErrorMsg(TopupDialogError.EMPTY_NUMBER);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_LENGTH_LESS_THAN_7_CHARACTERS);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_LENGTH_MORE_THAN_7_CHARACTERS);
    }

    @Test
    public void onTopupBtnClicked_EmptyAmount_ShowMissingAmountErrorMsg() {
        presenter.onTopupBtnClicked("7777777", "");

        verify(view).showAmountErrorMsg(TopupDialogError.EMPTY_AMOUNT);
    }

    @Test
    public void onTopupBtnClicked_AmountLengthMoreThan3_ShowAmountLengthErrorMsg() {
        presenter.onTopupBtnClicked("76655555", "4321");

        verify(view).showAmountErrorMsg(TopupDialogError.AMOUNT_LENGTH_MORE_THAN_3_CHARACTERS);
    }

    @Test
    public void onTopupBtnClicked_AmountValueIsLessThan5_ShowInvalidAmountErrorMsg() {
        presenter.onTopupBtnClicked("777775", "4");

        verify(view).showAmountErrorMsg(TopupDialogError.AMOUNT_LENGTH_LESS_THAN_5);
    }

    @Test
    public void onTopupBtnClicked_EmptyAmount_OnlyShowMissingAmountErrorMsg() {
        presenter.onTopupBtnClicked("7799999", "");

        verify(view, never()).showAmountErrorMsg(TopupDialogError.AMOUNT_LENGTH_MORE_THAN_3_CHARACTERS);
        verify(view, never()).showAmountErrorMsg(TopupDialogError.AMOUNT_LENGTH_LESS_THAN_5);
    }

    @Test
    public void onTopupBtnClicked_AmountLengthMoreThan3_OnlyShowAmountLengthErrorMsg() {
        presenter.onTopupBtnClicked("7444555", "4567");

        verify(view, never()).showAmountErrorMsg(TopupDialogError.EMPTY_AMOUNT);
        verify(view, never()).showAmountErrorMsg(TopupDialogError.AMOUNT_LENGTH_LESS_THAN_5);
    }

    @Test
    public void onTopupBtnClicked_NumberAndAmountIsCorrect_ReturnTopupToParentActivityWithNoErrorMsgs() {
        presenter.onTopupBtnClicked("7777766", "23");

        verifyNoErrorMessagesCalledFromOnAddTopupBtnClicked();
    }

    @Test
    public void onTopupBtnClicked_NumberAndAmountIsCorrect_ReturnTopupToParentActivity() {
        presenter.onTopupBtnClicked("7777777", "22");

        Topup topupDetails = new Topup("7777777", "22");
        verify(view).returnTopupToParentActivity(topupDetails);
    }

    @Test
    public void onTopupBtnClicked_InvalidNumber_DoNotCallReturnTopupToParentActivity() {
        presenter.onTopupBtnClicked("777776", "27");

        Topup topupDetails = new Topup("777776", "27");
        verify(view, never()).returnTopupToParentActivity(topupDetails);
    }

    @Test
    public void onTopupBtnClicked_InvalidAmount_DoNotCallReturnTopupToParentActivity() {
        presenter.onTopupBtnClicked("7777767", "2");

        Topup topupDetails = new Topup("777776", "27");
        verify(view, never()).returnTopupToParentActivity(topupDetails);
    }

    @Test
    public void onTopupBtnClicked_ValidTopup_DismissAddTopupDialog() {
        presenter.onTopupBtnClicked("7654321", "60");

        verify(view).dismissTopupDialog();
    }

    @Test
    public void onTopupBtnClicked_InvalidNumber_DismissAddTopupDialog() {
        presenter.onTopupBtnClicked("765432", "659");

        verify(view, never()).dismissTopupDialog();
    }

    @Test
    public void onTopupBtnClicked_InvalidAmount_DismissAddTopupDialog() {
        presenter.onTopupBtnClicked("765432", "6591");

        verify(view, never()).dismissTopupDialog();
    }

    @Test
    public void onCancelBtnClicked_NoInput_dismissAddTopupDialog() {
        presenter.onCancelBtnClicked();

        verify(view).dismissTopupDialog();
    }

    private void verifyNoErrorMessagesCalledFromOnAddTopupBtnClicked() {
        verify(view, never()).showNumberErrorMsg(TopupDialogError.EMPTY_NUMBER);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_LENGTH_LESS_THAN_7_CHARACTERS);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_LENGTH_MORE_THAN_7_CHARACTERS);
        verify(view, never()).showNumberErrorMsg(TopupDialogError.NUMBER_MUST_START_WITH_7);

        verify(view, never()).showAmountErrorMsg(TopupDialogError.EMPTY_AMOUNT);
        verify(view, never()).showAmountErrorMsg(TopupDialogError.AMOUNT_LENGTH_MORE_THAN_3_CHARACTERS);
        verify(view, never()).showAmountErrorMsg(TopupDialogError.AMOUNT_LENGTH_LESS_THAN_5);
    }

}
