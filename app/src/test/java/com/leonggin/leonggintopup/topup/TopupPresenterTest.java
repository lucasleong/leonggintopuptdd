package com.leonggin.leonggintopup.topup;

import com.leonggin.leonggintopup.data.Topup;
import com.leonggin.leonggintopup.topupdialog.TopupDialogContract;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class TopupPresenterTest {

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private TopupContract.View view;

    private TopupContract.Presenter presenter;

    @Mock
    private TopupContract.Repository repository;

    private Topup topupDetails;

    private List<Topup> topupDetailsList;

    @Before
    public void setUp() {
        presenter = new TopupPresenter(view, repository);

        topupDetails = new Topup("7777777", "22");
        topupDetails.setId(1);

        topupDetailsList = new ArrayList<>();
        topupDetailsList.add(topupDetails);
    }

    @Test
    public void onShowTopupDialogBtnClicked_NoInput_ShowTopupDialog() {
        presenter.onDoTopupBtnClicked();

        verify(view).showTopupDialog();
    }

    @Test
    public void onTopupBtnClicked_GivenValidTopup_SaveTopupToRepository() {
        ((TopupDialogContract.Listener) presenter).onTopupBtnClicked(topupDetails);

        verify(repository).cacheUncompletedTopup(topupDetails);
    }

    @Test
    public void onTopupBtnClicked_GivenValidTopup_DoTopup() {
        ((TopupDialogContract.Listener) presenter).onTopupBtnClicked(topupDetails);

        verify(view).doTopup(topupDetails);
    }

    @Test
    public void onDestroy_NoInput_CloseDbInstance() {
        presenter.onDestroy();

        verify(repository).closeDb();
    }

    @Test
    public void onCreate_NoInput_GetAllTopups() {
        presenter.onResume();

        verify(repository).getAllTopups();
    }

    @Test
    public void onPostCreate_NoInput_RefreshView() {
        when(repository.getAllTopups()).thenReturn(topupDetailsList);

        presenter.onResume();

        verify(view).refreshHistory(topupDetailsList);
    }

    @Test
    public void onSearchNumber_GivenTopupNumber_SearchTopup() {
        presenter.onSearchNumber("7777777");

        verify(repository).searchTopup("7777777");
    }

    @Test
    public void onSearchNumber_GivenTopupNumber_RefreshView() {
        when(repository.searchTopup("7777777")).thenReturn(topupDetailsList);

        presenter.onSearchNumber("7777777");

        verify(view).refreshHistory(topupDetailsList);
    }

}
